<?php

namespace Silex\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FormationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class)
            ->add('date', TextType::class)
            ->add('lieu', TextType::class)
            ->add('horaire', TextType::class)
            ->add('intervenant', TextType::class)
            ->add('public', TextType::class)
            ->add('objectif', TextType::class)
            ->add('description', TextareaType::class)
            ->add('prerequis', TextType::class)
            ->add('cout', TextType::class)
            ->add('coutRepas', TextType::class)
            ->add('dateLimiteInscription', TextType::class)
            ->add('dateFin', TypeText::class)
            ->add('duree', TypeText::class);
    }

    public function getName()
    {
        return 'formation';
    }
}
