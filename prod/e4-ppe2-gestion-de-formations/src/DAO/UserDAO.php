<?php

namespace Silex\DAO;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Silex\Domain\User;

class UserDAO extends DAO implements UserProviderInterface
{
    
    public function find($id) {
        $sql = "select * from utilisateur where user_id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("Pas d'utilisateur trouvé id " . $id);
    }

    public function loadUserByUsername($username)
    {
        $sql = "select * from utilisateur where user_nom=?";
        $row = $this->getDb()->fetchAssoc($sql, array($username));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new UsernameNotFoundException(sprintf('Utilisateur "%s" non trouvé.', $username));
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }


    public function supportsClass($class)
    {
        return 'Silex\Domain\User' === $class;
    }

    protected function buildDomainObject(array $row) {
        $user = new User();
        $user->setId($row['user_id']);
        $user->setUsername($row['user_nom']);
        $user->setPassword($row['user_password']);
        $user->setSalt($row['user_salt']);
        $user->setRole($row['user_role']);
        return $user;
    }
	
	 public function findAll() {
        $sql = "select * from utilisateur order by user_role, user_nom";
        $result = $this->getDb()->fetchAll($sql);
        $entities = array();
        foreach ($result as $row) {
            $id = $row['user_id'];
            $entities[$id] = $this->buildDomainObject($row);
        }
        return $entities;
    }
	
	 public function save(User $user) {
        $userData = array(
            'user_nom' => $user->getUsername(),
            'user_salt' => $user->getSalt(),
            'user_password' => $user->getPassword(),
            'user_role' => $user->getRole()
            );

        if ($user->getId()) {
            $this->getDb()->update('utilisateur', $userData, array('user_id' => $user->getId()));
        } else {
            $this->getDb()->insert('utilisateur', $userData);
            $id = $this->getDb()->lastInsertId();
            $user->setId($id);
        }
    }

    public function delete($id) {
        $this->getDb()->delete('utilisateur', array('user_id' => $id));
    }
}