<?php

	namespace Silex\DAO;
	use Silex\Domain\Formation;

	class FormationDAO extends DAO
	{
		
		public function findAll() 
		{
			$sql = "select * from formation order by form_id";
			$result = $this->getDb()->fetchAll($sql);
			$formations = array();
			foreach ($result as $row) {
				$formationId = $row['form_id'];
				$formations[$formationId] = $this->buildDomainObject($row);
			}
			return $formations;
		}
		
		protected function buildDomainObject(array $row) 
		{
			$formation = new Formation();
			$formation->setId($row['form_id']);
			$formation->setNom($row['nom']);
			$formation->setDate($row['date']);
			$formation->setHoraire($row['horaire']);
			$formation->setLieu($row['lieu']);
			$formation->setIntervenant($row['intervenant']);
			$formation->setPublic($row['public']);
			$formation->setObjectif($row['objectif']);
			$formation->setDescription($row['description']);
			$formation->setPrerequis($row['prerequis']);
			$formation->setCout($row['cout']);
			$formation->setCoutRepas($row['cout_repas']);
			$formation->setDateLimiteInscription($row['date_limite_inscription']);
			$formation->setDateFin($row['date_fin']);
			$formation->setDuree($row['duree']);
			return $formation;
		}
		
		public function find($id)
		{
			$sql="select * from formation where form_id=?";
			$row = $this->getDb()->fetchAssoc($sql, array($id));
			if($row)
				return $this->buildDomainObject($row);
			else
				throw new \Exception("Pas de formation trouvé à cet id".$id);
		}
		
		 public function save(Formation $formation) {
			$formationData = array(
            'nom' => $formation->getNom(),
            'description' => $formation->getDescription(),
            );

        if ($formation->getId()) {
            $this->getDb()->update('formation', $formationData, array('form_id' => $formation->getId()));
        } else {
            $this->getDb()->insert('formation', $formationData);
            $id = $this->getDb()->lastInsertId();
            $formation->setId($id);
        }
		}	

    public function delete($id) {
        $this->getDb()->delete('formation', array('form_id' => $id));
	}
	}
