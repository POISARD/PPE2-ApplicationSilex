<?php

	namespace Silex\DAO;

	use Silex\Domain\Commentaire;

	class CommentaireDAO extends DAO 
	{
		private $formationDAO;
		private $userDAO;

		public function setFormationDAO(FormationDAO $formationDAO) {
			$this->formationDAO = $formationDAO;
		}

		public function setUserDAO(UserDAO $userDAO) {
			$this->userDAO = $userDAO;
		}

		public function findAllByFormation($formationId) {
			
			$formation = $this->formationDAO->find($formationId);
			$sql = "select * from commentaire where form_id=? order by com_id";
			$result = $this->getDb()->fetchAll($sql, array($formationId));
			$commentaires = array();
			foreach ($result as $row) {
				$commentaireId = $row['com_id'];
				$commentaire = $this->buildDomainObject($row);
				$commentaire->setFormation($formation);
				$commentaires[$commentaireId] = $commentaire;
			}
			return $commentaires;
		}

		protected function buildDomainObject(array $row) {
			$commentaire = new Commentaire();
			$commentaire->setId($row['com_id']);
			$commentaire->setContent($row['contenu']);

			if (array_key_exists('form_id', $row)) {
				$formationId = $row['form_id'];
				$formation = $this->formationDAO->find($formationId);
				$commentaire->setFormation($formation);
			}
			if (array_key_exists('user_id', $row)) {
				$userId = $row['user_id'];
				$user = $this->userDAO->find($userId);
				$commentaire->setAuteur($user);
			}
			return $commentaire;
		}
		
		public function save(Commentaire $comment) {
			$commentData = array(
            'form_id' => $comment->getFormation()->getId(),
            'user_id' => $comment->getAuteur()->getId(),
            'contenu' => $comment->getContent(),
            'date' => $comment->getDate
            );

        if ($comment->getId()) {
            $this->getDb()->update('commentaire', $commentData, array('com_id' => $comment->getId()));
        } else {
            $this->getDb()->insert('commentaire', $commentData);
            $id = $this->getDb()->lastInsertId();
            $comment->setId($id);
        }
    }
	
	public function findAll() {
        $sql = "select * from commentaire order by com_id desc";
        $result = $this->getDb()->fetchAll($sql);
        $entities = array();
        foreach ($result as $row) {
            $id = $row['com_id'];
            $entities[$id] = $this->buildDomainObject($row);
        }
        return $entities;
    }
	
	
	 public function deleteAllByFormation($formationId) {
        $this->getDb()->delete('commentaire', array('form_id' => $formationId));
    }
	
	 public function find($id) {
        $sql = "select * from commentaire where com_id=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("Pas de commentaire trouvé à cet id " . $id);
    }

   
    public function delete($id) {
        $this->getDb()->delete('commentaire', array('com_id' => $id));
    }
	
	public function deleteAllByUser($userId) {
        $this->getDb()->delete('commentaire', array('user_id' => $userId));
	}
	
}
