<?php

	namespace Silex\Domain;

	class Formation
	{
		private $id;
		private $nom;
		private $date;
		private $horaire;
		private $lieu;
		private $intervenant;
		private $public;
		private $objectif;
		private $description;
		private $prerequis;
		private $cout;
		private $coutRepas;
		private $dateLimiteInscription;
		private $dateFin;
		private $duree;

		public function getId() {
			return $this->id;
		}

		public function setId($id) {
			$this->id = $id;
			return $this;
		}

		public function getNom() {
			return $this->nom;
		}

		public function setNom($nom) {
			$this->nom = $nom;
			return $this;
		}
		
		public function getDate() {
			return $this->date;
		}

		public function setDate($date) {
			$this->date = $date;
			return $this;
		}
		
		public function getHoraire() {
			return $this->horaire;
		}

		public function setHoraire($horaire) {
			$this->horaire = $horaire;
			return $this;
		}
		
		public function getLieu() {
			return $this->lieu;
		}

		public function setLieu($lieu) {
			$this->lieu = $lieu;
			return $this;
		}
		
		public function getIntervenant() {
			return $this->intervenant;
		}

		public function setIntervenant($intervenant) {
			$this->intervenant = $intervenant;
			return $this;
		}
		
		public function getPublic() {
			return $this->public;
		}

		public function setPublic($public) {
			$this->public = $public;
			return $this;
		}
		
		public function getObjectif() {
			return $this->objectif;
		}

		public function setObjectif($objectif) {
			$this->objectif = $objectif;
			return $this;
		}
		
		public function getDescription() {
			return $this->description;
		}

		public function setDescription($description) {
			$this->description = $description;
			return $this;
		}
		
		public function getPrerequis() {
			return $this->prerequis;
		}

		public function setPrerequis($prerequis) {
			$this->prerequis = $prerequis;
			return $this;
		}
		
		public function getCout() {
			return $this->cout;
		}

		public function setCout($cout) {
			$this->cout = $cout;
			return $this;
		}
		
		public function getCoutRepas() {
			return $this->coutRepas;
		}

		public function setCoutRepas($coutRepas) {
			$this->coutRepas = $coutRepas;
			return $this;
		}
		
		public function getDateLimiteInscription() {
			return $this->dateLimiteInscription;
		}

		public function setDateLimiteInscription($dateLimiteInscription) {
			$this->dateLimiteInscription = $dateLimiteInscription;
			return $this;
		}
		
		public function getDateFin(){
			return $this->dateFin;
		}
		
		public function setDateFin($dateFin){
			$this->dateFin = $dateFin;
			return $this;
		}
		
		public function getDuree(){
			return $this->duree;
		}
		
		public function setDuree($duree){
			$this->duree = $duree;
			return $this;
		}
		
		
	}
?>
