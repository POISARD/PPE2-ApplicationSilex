<?php

namespace Silex\Domain;

class Commentaire 
{
    private $id;
    private $auteur;
    private $content;
    private $formation;
    private $date;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getAuteur() {
        return $this->auteur;
    }

    public function setAuteur(User $auteur) {
        $this->auteur = $auteur;
        return $this;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function getFormation() {
        return $this->formation;
    }

    public function setFormation(Formation $formation) {
        $this->formation = $formation;
        return $this;
    }
    
    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this;
    }
}
