﻿SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pressilex`
--
CREATE DATABASE IF NOT EXISTS `pressilex` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pressilex`;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `contenu` varchar(500) NOT NULL,
  `form_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`com_id`),
  KEY `fk_com_art` (`form_id`),
  KEY `fk_com_usr` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`com_id`, `contenu`, `form_id`, `user_id`) VALUES
(1, 'Silex est fait en PHP !', 2, 1),
(2, 'En effet, car Silex est basé sur Symfony !', 2, 2),
(3, 'Qui lui aussi est fait en PHP !', 2, 1),
(4, 'Le Java peut aussi être utilisé pour faire du web !', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

DROP TABLE IF EXISTS `formation`;
CREATE TABLE IF NOT EXISTS `formation` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) DEFAULT NULL,
  `date` varchar(250) DEFAULT NULL,
  `horaire` varchar(250) DEFAULT NULL,
  `lieu` varchar(1000) DEFAULT NULL,
  `intervenant` varchar(150) DEFAULT NULL,
  `public` varchar(250) DEFAULT NULL,
  `objectif` varchar(250) DEFAULT NULL,
  `description` text,
  `prerequis` varchar(250) DEFAULT NULL,
  `cout` varchar(250) DEFAULT NULL,
  `cout_repas` varchar(250) DEFAULT NULL,
  `date_limite_inscription` varchar(100) DEFAULT NULL,
  `date_fin` varchar(50) DEFAULT NULL,	
  `duree` varchar(50) DEFAULT NULL,	
  PRIMARY KEY (`form_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`form_id`, `nom`,`date`, `horaire`, `lieu`, `intervenant`, `public`, `objectif`, `description`, `prerequis`, `cout`, `cout_repas`, `date_limite_inscription`, `duree`, `date_fin`) VALUES
(	1,
	'Formation Publisher',
	'Jeudi 15 décembre 05 et jeudi 5 Janvier 06',
	'9h à 17h',
	'Salle informatique Maison Régionale des Sports de Lorraine 13 Rue Jean Moulin 54 510 Tomblaine',
	'Jérôme Poe RMI informatique',
	'Bénévoles, dirigeants et salariés du mouvement sportif',
	'Savoir créer et produire des compositions de type brochure, prospectus, formulaire, invitation, bulletin d’informations',
	'Partie 1 : Typographie > Règles de base > Mise en page et composition > Quelques exemples conseillés et déconseillés > Partie 2 : La page > Formatage (marge, colonne,…) > Composition > Les différents objets dans la page > Partie 3 : Texte > Création et placement d’un texte > Les modes de coulage d’un texte dans les colonnes > Édition de texte (couper, copier, coller) > Caractères et polices > Paragraphe/Tabulation > Manipulation des cadres de textes > Création de tableaux > Partie 4 : Image > Importation (bitmap/vectoriel) > Manipulation et déformation > Rognage (découpage d’image) > Partie 5 : Les effets spéciaux de Wordart > La maquette > Préparation > Maquette recto/verso > Liens dynamiques avec le document > Formats particuliers de texte, graphiques et images > Chaînage et déroulement des blocs-textes > Orientation des blocs > Insertion, déplacement et suppression de pages > Mise en œuvre des publications > Présentation des Assistants Publisher > Partie 6 : Objets graphiques > Lignes, cercles, rectangles > Type de ligne et de surface > Avant plan et arrière plan > Partie 7 : Composition > Placer les textes et les images en fonction des uns des autres > Habillage d’une image > En-tête et pied de page > Utilisation des repères > Utilisation de l’arrière plan > Foliotage (pagination) > Imprimer le document > Utilisation de modèles prédéfinis',
	'Maîtriser les commandes de base de Windows',
	'110€ support de cours inclus',
	'A charge des participants',
	'25 novembre 05',
	'2 journées',
	'11 octobre 2018'),
(	2,
	'Formation Powerpoint Niveau 1 - SAMEDI',
	'Samedi 17 décembre 2005',
	'9h-12h et 13h30-17h',
	'Salle informatique Maison Régionale des Sports de Lorraine 13 Rue Jean Moulin 54 510 Tomblaine',
	'Jérôme Poe RMI informatique',
	'Bénévoles, dirigeants et salariés du mouvement sportif',
	'Mettre en œuvre les différentes commandes et les fonctions de PowerPoint.',
	'Partie 1 : Découverte et prise en main de PowerPoint > Qu’est-ce que la Présentation Assistée par Ordinateur ? > Présentation de l’écran et des boîtes de dialogue > Terminologie des objets et des fonctionnalités > Personnalisation de la barre d’outils > Les assistants, les menus contextuels > Partie 2 : Création d’une présentation : mise en page des diapos > Diapos listes à puces > Utilisation de la règle > Correcteur orthographique > Insertion d’images, titres WordArt > Insertion de tableaux,  Insertion de graphiques > Insertion d’organigramme hiérarchique > Réalisation de figures géométriques et formes prédéfinies > La mise en forme des objets > La hiérarchie et l’association des objets > Partie 4 : Les différents modes d’affichage > Mode Diapo,  Mode Normal, Mode Plan > Mode Trieuse de diapos > Mode Diaporama > Partie 4 : Intégration de diapos extérieures > Diapositives de fichier > Diapositives de plan > Partie 5 : Mise en page du diaporama et impression du document > En-tête et pied de page du document > En-tête et pied de page des diapositives > Modifier le masque des diapositives > Insertion de diapos de résumé > Masquer des diapos > Partie 6 : Les animations > Effets de transition > Effets d’animation : animer du texte, des objets, des graphiques, insérer des sons > Exporter des diaporamas > Exécuter des diaporamas > Visionneuse > Diaporama personnalisé > Partie 7 : Les modèles > Créer ses modèles personnalisés > Utiliser les modèles intégrés > Partie 8 : Powerpoint et Internet > Créer des liens hypertexte > Enregistrer au format HTML',
	'Personnes réalisant des transparents, graphiques ou diaporamas. Programme sur 1 jour : bonne connaissance de Windows, Word et Excel.',
	'55 € support de cours inclus',
	'A charge des participants',
	'Vendredi 2 décembre 05',
	'1 journée',
	'11 octobre 2018'),
(	3,
	'Formation Outlook Niveau 1 - SAMEDI',
	'Samedi 26 novembre 05',
	'9h-12h et 13h30-17h',
	'Salle informatique MRSL',
	'Jérôme Poe RMI informatique',
	'Bénévoles, dirigeants et salariés du mouvement sportif',
	'Savoir gérer les différents objets et les outils de communication',
	'Partie 1 : Outlook > Présentation et démarrage > Le navigateur > Partie 2 : Envoi de messages > Composition > Le carnet d’adresses > Rédaction d’un message > Mise en forme > L’insertion de pièces jointes depuis le message ou d’une application > Options d’envoi de messages > Enregistrer un message inachevé > Récupérer un message inachevé et l’envoyer > Insérer des repères d’importance et suivi des messages > Ajouter un indicateur de suivi à un message > Définir l’importance d’un message > Demander une réponse et une confirmation de lecture > Utiliser les boutons de vote > Désigner un autre utilisateur comme destinataire des réponses à votre message > Ajouter un lien hypertexte à un message > Signatures des messages > Partie 3 : Ouverture de messages > Caractéristiques d’un message > Répondre à l’expéditeur, répondre à tous > Transfert de messages > Partie 4 : Gestion du carnet d’adresses > Carnet d’adresse personnel > Créer une liste de diffusion > Partie 6 : Gestion des messages > Suppression, recherche, tri et impression > Archivage de messages > Signatures automatiques > Partie 6 : Contacts > Les modes d’affichage > Utilisation dans l’agenda, dans Office > Gestion des contacts > Partie 7 : Calendrier > Les modes d’affichage > Création de rendez-vous / Évènements > Caractéristiques, propriétés, alarme > Gestion des rendez-vous / Évènements',
	'Personnels souhaitant maîtriser les options de base d’Outlook.',
	'55 € support de cours inclus',
	'A charge des participants',
	'10 novembre 05',
	'1 journée',
	'11 octobre 2018'),
(	4,
	'Formation Powerpoint Niveau 1',
	'Jeudi 10 novembre 05',
	'9h - 12h et 13h30 - 17h',
	'Salle informatique MRSL',
	'Jérôme Poe RMI Informatique',
	'Bénévoles, dirigeants et salariés du mouvement sportif',
	'Mettre en oeuvre les différentes commandes et les fonctions de powerpoint',
	'Partie 1 :  Découverte et prise en main de PowerPoint > Qu’est-ce que la Présentation Assistée par Ordinateur ? > Présentation de l’écran et des boîtes de dialogue > Terminologie des objets et des fonctionnalités > Personnalisation de la barre d’outils > Les assistants, les menus contextuels > Partie 2 : Création d’une présentation : mise en page des diapos > Diapos listes à puces > Utilisation de la règle > Correcteur orthographique > Insertion d’images, titres WordArt > Insertion de tableaux,  Insertion de graphiques > Insertion d’organigramme hiérarchique > Réalisation de figures géométriques et formes prédéfinies > La mise en forme des objets > La hiérarchie et l’association des objets > Partie 3 : Les différents modes d’affichage > Mode Diapo,  Mode Normal, Mode Plan > Mode Trieuse de diapos > Mode Diaporama > Partie 4 : Intégration de diapos extérieures > Diapositives de fichier > Diapositives de plan > Partie 5 : Mise en page du diaporama et impression du document > En-tête et pied de page du document > En-tête et pied de page des diapositives > Modifier le masque des diapositives > Insertion de diapos de résumé > Masquer des diapos > Partie 6 : Les animations > Effets de transition > Effets d’animation : animer du texte, des objets, des graphiques, insérer des sons > Exporter des diaporamas > Exécuter des diaporamas > Visionneuse > Diaporama personnalisé > Partie 7 : Les modèles > Créer ses modèles personnalisés > Utiliser les modèles intégrés > Partie 8 : Powerpoint et Internet > Créer des liens hypertexte > Enregistrer au format HTML ',
	'Personnes réalisant des transparents, graphiques ou diaporamas. Programme sur 1 jour : bonne connaissance de Windows, Word et Excel.',
	'55 € support de cours inclus ',
	'A charge des participants',
	'31 octobre 05',
	'1 journée',
	'11 décembre 2018'),
(	5,
	'Formation Powerpoint Niveau 2',
	'Mardi 15 novembre 05',
	'9h-12h et 13h30 - 17h',
	'Salle informatique Maison Régionale des Sports de Lorraine 13 Rue Jean Moulin 54 510 Tomblaine ',
	'Jérôme Poe RMI informatique ',
	'Bénévoles, dirigeants et salariés du mouvement sportif',
	'Parfaire ses connaissances sur PowerPoint',
	'Partie 1 : Amélioration d’une présentation > Manipulation des objets > Insertions d’effets > Les figures géométriques, modification et personnalisation > Association d’objets > Partie 2 : L’affichage > Les différents modes d’affichage > Partie 3 : Personnalisation des diapositives > Appliquer un modèle de présentation > Jeu des couleurs > Partie 4 : Les modèles > Appliquer un modèle de présentation > Partie 5 : Les animations > Les effets du diaporama > Ajouter des animations > Animer un texte > Animer un graphique > Présenter un diaporama > Partie 6 : Enregistrer une présentation > Enregistrer un diaporama > Présentation à emporter > Exécuter une présentation sur un autre ordinateur > Diaporama personnalisé > Partie 7 : Ajouter du son > Partie 8 : PowerPoint et Internet',
	'Windows',
	'55 € support de cours inclus',
	'A charge des participants',
	'31 octobre 05',
	'1 journée',
	'11 octobre 2018'),
(	6,
	'Formation Photoshop',
	'Jeudi 1er et 8 décembre 05',
	'9h – 17h',
	'Salle informatique Maison Régionale des Sports de Lorraine 13 Rue Jean Moulin 54 510 Tomblaine',
	'Jérôme Poe RMI informatique',
	'Bénévoles, dirigeants et salariés du mouvement sportif désirant s’ouvrir aux techniques de traitement informatique de l’image ainsi qu’à la pratique de la photographie numérique',
	'Découvrir le traitement des images numériques couleur ainsi que leur séparation quadrichromique. Répondre aux besoins des photographes, photograveurs, des créatifs et des inventeurs d’images. Acquérir une méthode de travail professionnelle',
	'Partie 1 : Rappels sur les images numériques > Le pixel, la résolution d’une image ppp > Principes généraux de l’acquisition > Étalonnage de l’écran > Partie 2 : Les modes colorimétriques > Sources lumineuses et caractéristiques d’une couleur > Niveaux de gris et couleurs indexées > Synthèse additive RVB et synthèse soustractive CMJN > Méthodes de réduction du nombre de couleurs > Interpolation bilinéaire et bicubique > Partie 3 : Présentation et personnalisation > Modules externes et disques de travail > Paramétrage de la mémoire cache > Préférences d’affichage, unités de règles de repères et de grille > Les options de la palette : formes, couleurs, calques, historiques,... > Les options de la boîte à outils : couleurs, mode masque,... > Manipulation des outils > Partie 4 : Traitement numérique et retouche partielle > Recadrage, dimension, définition et taille d’une image > Sélection partielle d’une image > Retouche de la luminosité et du contraste d’une image, dosage des couleurs, variation de la teinte et saturation, réglage du “gamma”, notions de masque, netteté d’une image et “piqué” > Partie 5 : Travaux photographiques > Transformations d’images : symétrie, homothétie, rotation et anamorphose > Création et manipulation des calques, effets de transparence et effets de calques > Utilisations des filtres > Partie 6 : Les formats d’échange > Les formats PSD, PDD > Les différents formats d’enregistrement (EPS, TIFF, JPG, PDF, DCS,...) > Importation et exportation > Partie 7 : Principes de base d’impression > Les différentes possibilités d’impression, couleurs non imprimables, séparation de couleurs > Partie 8 : Mises en pratique et capacités > Retoucher et réparer des images > Créer des formes et modifier les sélections avec la plume et les outils associés > Appliquer des effets spéciaux > Préparer les images pour l’impression > Optimiser les images pour le Web',
	'une bonne pratique de l’outil informatique est indispensable ; la connaissance d’autres logiciels de la chaîne graphique est souhaitée.',
	'110 € support de cours inclus',
	'A charge des participants',
	'18 novembre 05',
	'2 journées',
	'11 novembre 2018');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_nom` varchar(50) NOT NULL,
  `user_password` varchar(88) NOT NULL,
  `user_salt` varchar(23) NOT NULL,
  `user_role` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`user_id`, `user_nom`, `user_password`, `user_salt`, `user_role`) VALUES
(1, 'JohnDoe', '$2y$13$F9v8pl5u5WMrCorP9MLyJeyIsOLj.0/xqKd/hqa5440kyeB7FQ8te', 'YcM=A$nsYzkyeDVjEUa7W9K', 'ROLE_USER'),
(2, 'JaneDoe', '$2y$13$qOvvtnceX.TjmiFn4c4vFe.hYlIVXHSPHfInEG21D99QZ6/LM70xa', 'dhMTBkzwDKxnD;4KNs,4ENy', 'ROLE_USER'),
(3, 'admin', '$2y$13$A8MQM2ZNOi99EW.ML7srhOJsCaybSbexAj/0yXrJs4gQ/2BqMMW2K', 'EDDsl&fBCJB|a5XUtAlnQN8', 'ROLE_ADMIN');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
