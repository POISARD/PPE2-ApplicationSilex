<?php

namespace Silex\Tests;

require_once __DIR__.'/../../vendor/autoload.php';

use Silex\WebTestCase;

class AppTest extends WebTestCase
{
    public function testPageIsSuccessful($url)
    {
        $client = $this->createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function createApplication()
    {
        $app = new \Silex\Application();

        require __DIR__.'/../../app/config/dev.php';
        require __DIR__.'/../../app/app.php';
        require __DIR__.'/../../app/routes.php';
        
        unset($app['exception_handler']);
        $app['session.test'] = true;
        $app['security.access_rules'] = array();

        return $app;
    }

    public function provideUrls()
    {
        return array(
            array('/web'),
            array('/web/langage/1'),
            array('/web/login'),
            array('/web/admin'),
            array('/web/admin/langage/add'),
            array('/web/admin/langage/1/edit'),
            array('/web/admin/comment/1/edit'),
            array('/web/admin/user/add'),
            array('/web/admin/user/1/edit'),
			array('/api/langages'),
            array('/api/langage/1'),
            ); 
    }
	
	
}