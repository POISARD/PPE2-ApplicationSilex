<?php

	// Doctrine (db)
	$app['db.options'] = array(
		'driver'   => 'pdo_mysql',
		'charset'  => 'utf8',
		'host'     => 'localhost',
		'port'     => '3306',
		'dbname'   => 'axopen',
		'user'     => 'root',
		'password' => '',
	);
	
	$app['monolog.level'] = 'WARNING';
?>