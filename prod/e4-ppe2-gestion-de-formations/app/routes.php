<?php

	use Symfony\Component\HttpFoundation\Request;
	use Silex\Domain\Commentaire;
	use Silex\Domain\Formation;
	use Silex\Domain\User;
	use Silex\Form\Type\CommentType;
	use Silex\Form\Type\FormationType;
	use Silex\Form\Type\UserType;

	// Home page
	$app->get('/', function () use ($app) {
		return $app['twig']->render('index.html.twig', array());
	})->bind('home');
	
	// Affichage de toutes les formations
	$app->get('/formations', function () use ($app) {
		$formations = $app['dao.formation']->findAll();
		return $app['twig']->render('formations.html.twig', array('formations' => $formations));
	})->bind('formations');

	// Formation avec commentaire
	$app->match('/formation/{id}', function ($id, Request $request) use ($app) {
		$formation = $app['dao.formation']->find($id);
		//Mise en forme des descriptions de formation
		// Souci d'interpretation du code HTML, non interprete mais affiche
		/*$final = array();
		$formations = explode(">",$formation->getDescription());
		foreach($formations as $formationUnite){
			array_push($final,$formationUnite."<br>");
		}
		$formation->setDescription(implode("",$final));*/
		$commentFormView = null;
		if ($app['security.authorization_checker']->isGranted('IS_AUTHENTICATED_FULLY')) {
			$comment = new Commentaire();
			$comment->setFormation($formation);
			$user = $app['user'];
			$comment->setAuteur($user);
			$commentForm = $app['form.factory']->create(CommentType::class, $comment);
			$commentForm->handleRequest($request);
			if ($commentForm->isSubmitted() && $commentForm->isValid()) {
				$app['dao.commentaire']->save($comment);
				$app['session']->getFlashBag()->add('success', 'Le commentaire a bien été ajouté');
			}
			$commentFormView = $commentForm->createView();
		}
		$commentaires = $app['dao.commentaire']->findAllByFormation($id);

		return $app['twig']->render('formation.html.twig', array(
			'formation' => $formation, 
			'commentaires' => $commentaires,
			'commentForm' => $commentFormView));
	})->bind('formation');

	// Login form
	$app->get('/login', function(Request $request) use ($app) {
		return $app['twig']->render('login.html.twig', array(
			'error'         => $app['security.last_error']($request),
			'last_username' => $app['session']->get('_security.last_username'),
		));
	})->bind('login');

//-------------------------------BACK OFFICE --------------
	// Administration
	$app->get('/admin', function() use ($app) {
		$formations = $app['dao.formation']->findAll();
		$comments = $app['dao.commentaire']->findAll();
		$users = $app['dao.user']->findAll();
		return $app['twig']->render('admin.html.twig', array(
			'formations' => $formations,
			'comments' => $comments,
			'users' => $users));
	})->bind('admin');
	
	//Ajout
	$app->match('/admin/formation/add', function(Request $request) use ($app) {
    $formation = new Formation();
    $formationForm = $app['form.factory']->create(FormationType::class, $formation);
    $formationForm->handleRequest($request);
    if ($formationForm->isSubmitted() && $formationForm->isValid()) {
        $app['dao.formation']->save($formation);
        $app['session']->getFlashBag()->add('success', 'La nouvelle formation a été ajouté.');
    }
    return $app['twig']->render('formation_form.html.twig', array(
        'title' => 'Nouvelle formation',
        'formationForm' => $formationForm->createView()));
})->bind('admin_formation_add');

//Modification
$app->match('/admin/formation/{id}/edit', function($id, Request $request) use ($app) {
    $formation = $app['dao.formation']->find($id);
    $formationForm = $app['form.factory']->create(FormationType::class, $formation);
    $formationForm->handleRequest($request);
    if ($formationForm->isSubmitted() && $formationForm->isValid()) {
        $app['dao.formation']->save($formation);
        $app['session']->getFlashBag()->add('success', 'La formation a bien été mis à jour.');
    }
    return $app['twig']->render('formation_form.html.twig', array(
        'title' => 'Modifier une formation',
        'formationForm' => $formationForm->createView()));
})->bind('admin_formation_edit');

// Suppression
$app->get('/admin/formation/{id}/delete', function($id, Request $request) use ($app) {
    // Supprimer tous les commentaires associés
    $app['dao.commentaire']->deleteAllByFormation($id);
    // Supprimer la formation
    $app['dao.formation']->delete($id);
    $app['session']->getFlashBag()->add('success', 'La formation a bien été supprimé.');
    // Redirection sur la page administration
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_formation_delete');

//Editer un commentaire
$app->match('/admin/comment/{id}/edit', function($id, Request $request) use ($app) {
    $comment = $app['dao.commentaire']->find($id);
    $commentForm = $app['form.factory']->create(CommentType::class, $comment);
    $commentForm->handleRequest($request);
    if ($commentForm->isSubmitted() && $commentForm->isValid()) {
        $app['dao.commentaire']->save($comment);
        $app['session']->getFlashBag()->add('success', 'Le commentaire a bien été mis à jour.');
    }
    return $app['twig']->render('comment_form.html.twig', array(
        'title' => 'Editer le commentaire',
        'commentForm' => $commentForm->createView()));
})->bind('admin_comment_edit');

// Supprimer un commentaire
$app->get('/admin/comment/{id}/delete', function($id, Request $request) use ($app) {
    $app['dao.commentaire']->delete($id);
    $app['session']->getFlashBag()->add('success', 'Le commentaire a bien été supprimé.');
    // Redirection sur la page d'administration
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_comment_delete');

// Ajou
$app->match('/admin/user/add', function(Request $request) use ($app) {
    $user = new User();
    $userForm = $app['form.factory']->create(UserType::class, $user);
    $userForm->handleRequest($request);
    if ($userForm->isSubmitted() && $userForm->isValid()) {
        // generate a random salt value
        $salt = substr(md5(time()), 0, 23);
        $user->setSalt($salt);
        $plainPassword = $user->getPassword();
        // find the default encoder
        $encoder = $app['security.encoder.bcrypt'];
        // compute the encoded password
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setPassword($password); 
        $app['dao.user']->save($user);
        $app['session']->getFlashBag()->add('success', 'L\'utilisateur a bien été créé.');
    }
    return $app['twig']->render('user_form.html.twig', array(
        'title' => 'Nouvel Utilisateur',
        'userForm' => $userForm->createView()));
})->bind('admin_user_add');

// Modifier un utilisateur
$app->match('/admin/user/{id}/edit', function($id, Request $request) use ($app) {
    $user = $app['dao.user']->find($id);
    $userForm = $app['form.factory']->create(UserType::class, $user);
    $userForm->handleRequest($request);
    if ($userForm->isSubmitted() && $userForm->isValid()) {
        $plainPassword = $user->getPassword();
        $encoder = $app['security.encoder_factory']->getEncoder($user);
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setPassword($password); 
        $app['dao.user']->save($user);
        $app['session']->getFlashBag()->add('success', 'L\'utilisateur a bien été modifié.');
    }
    return $app['twig']->render('user_form.html.twig', array(
        'title' => 'Modifier l\'utilisateur',
        'userForm' => $userForm->createView()));
})->bind('admin_user_edit');

// Supprimer un utilisateur
$app->get('/admin/user/{id}/delete', function($id, Request $request) use ($app) {
    // Supprimer les commentaires associés
    $app['dao.commentaire']->deleteAllByUser($id);
    // Supprimer l'utilisateur
    $app['dao.user']->delete($id);
    $app['session']->getFlashBag()->add('success', 'L\'utilisateur a bien été supprimé.');
    // Redirection sur la page d'administration
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_user_delete');

//----------------------------------API-----------------------------


	// API : Récup toutes les formations
	$app->get('/api/formations', function() use ($app) {
		$formations = $app['dao.formation']->findAll();
		$responseData = array();
		foreach ($formations as $formation) {
			$responseData[] = array(
				'id' => $formation->getId(),
				'nom' => $formation->getNom(),
				'description' => $formation->getDescription()
				);
		}
		return $app->json($responseData);
	})->bind('api_formations');

	// API : Récup une seule formation
	$app->get('/api/formation/{id}', function($id) use ($app) {
		$formation = $app['dao.formation']->find($id);
		$responseData = array(
			'id' => $formation->getId(),
			'nom' => $formation->getNom(),
			'description' => $formation->getDescription()
			);
		return $app->json($responseData);
	})->bind('api_formation');
	
	// API : Création nouvelle formation
	$app->post('/api/formation', function(Request $request) use ($app) {
    // Check request parameters
    if (!$request->request->has('nom')) {
        return $app->json('Paramètre manquant : nom', 400);
    }
    if (!$request->request->has('description')) {
        return $app->json('Paramètre manquant : description ', 400);
    }
    // Créér et sauvegarder une formation
    $formation = new Formation();
    $formation->setNom($request->request->get('nom'));
    $formation->setDescription($request->request->get('description'));
    $app['dao.formation']->save($formation);
    $responseData = array(
        'id' => $formation->getId(),
        'nom' => $formation->getNom(),
        'description' => $formation->getDescription()
        );
    return $app->json($responseData, 201);  // 201 = Création
})->bind('api_formation_add');

// API : Suppression d'une formation existante
$app->delete('/api/formation/{id}', function ($id, Request $request) use ($app) {
    // Suppression des commentaires associés
    $app['dao.comment']->deleteAllByFormation($id);
    // Suppression de la formation
    $app['dao.formation']->delete($id);
    return $app->json('Pas de contenu', 204);  // 204 = No content
})->bind('api_formation_delete');

