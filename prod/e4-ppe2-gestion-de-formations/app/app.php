<?php

	use Symfony\Component\Debug\ErrorHandler;
	use Symfony\Component\Debug\ExceptionHandler;
	use Symfony\Component\HttpFoundation\Request;

	// Register global error and exception handlers
	ErrorHandler::register();
	ExceptionHandler::register();

	// Register service providers
	$app->register(new Silex\Provider\DoctrineServiceProvider());
	$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__.'/../views',
	));
	$app['twig'] = $app->extend('twig', function(Twig_Environment $twig, $app) {

		$twig->addExtension(new Twig_Extensions_Extension_Text());

		return $twig;

	});
	$app->register(new Silex\Provider\SecurityServiceProvider(), array(
		'security.firewalls' => array(
			// ...
		),
		'security.role_hierarchy' => array(
			'ROLE_ADMIN' => array('ROLE_USER'),
		),
		'security.access_rules' => array(
			array('^/admin', 'ROLE_ADMIN'),
		),
	));
	$app->register(new Silex\Provider\ValidatorServiceProvider());
	$app->register(new Silex\Provider\AssetServiceProvider(), array(
		'assets.version' => 'v1'
	));
	$app->register(new Silex\Provider\SessionServiceProvider());
	$app->register(new Silex\Provider\SecurityServiceProvider(), array(
		'security.firewalls' => array(
			'secured' => array(
				'pattern' => '^/',
				'anonymous' => true,
				'logout' => true,
				'form' => array('login_path' => '/login', 'check_path' => '/login_check'),
				'users' => function () use ($app) {
					return new Silex\DAO\UserDAO($app['db']);
				},
			),
		),
	));
	$app->register(new Silex\Provider\FormServiceProvider());
	$app->register(new Silex\Provider\LocaleServiceProvider());
	$app->register(new Silex\Provider\TranslationServiceProvider());
	$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/silex.log',
    'monolog.name' => 'Silex',
    'monolog.level' => $app['monolog.level']
));
	$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    switch ($code) {
        case 403:
            $message = 'Accès refusé.';
            break;
        case 404:
            $message = 'La page n\'a pas pu être trouvée.';
            break;
        default:
            $message = "Aïe ! Il y a eu un problème.";
    }
    return $app['twig']->render('error.html.twig', array('message' => $message));
});

//-------- 

	$app['dao.formation'] = function($app) {
		return new Silex\DAO\FormationDAO($app['db']);
	};
	$app['dao.user'] = function ($app) {
		return new Silex\DAO\UserDAO($app['db']);
	};
	$app['dao.commentaire'] = function ($app) {
		$commentaireDAO = new Silex\DAO\CommentaireDAO($app['db']);
		$commentaireDAO->setFormationDAO($app['dao.formation']);
		$commentaireDAO->setUserDAO($app['dao.user']);
		return $commentaireDAO;
	};
//---- JSON
	
	$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});