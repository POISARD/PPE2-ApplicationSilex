# PPE2 - Application web sous le micro-framework Silex

L'application est accompagnée de toute sa documentation ainsi que les informations nécessaires à l'utilisation d'outils (gitlab). Les répertoires sont :

+ **Documentation**

La documentation donnée est celle faite par le groupe d'élève, il n'y avait pas de sujet car nous avions développé (pour nous entraîner) une autre application Silex quasi identique renseignant sur les cours et conférences. C'est sur l'application des cours et conférences que sont basés les .json exploités par l'application Android Java.

L'application fût donc développée dans le but de reproduire le fonctionnement de l'ancienne version présentant les cours et conférences, mais dans l'actuelle nous présentons des formations.

+ **Prod**
  * e4-ppe2-gestion-de-formations (dossier git d'origine mais je l'inclus à un nouveau gitlab pour vous donner aussi les documentations)

La Production consiste en un dossier contenant les sources de l'application web Silex, et d'une archive contenant ce même dossier.

## Exploitation - déploiement

L'application Silex demande quelques modifications pour fonctionner, les voici :

+ **La base de données**

Tout d'abord, veuillez créer une base de donnée MySQL, sous utf-8. Puis, grâce au script (dump sql du phpmyadmin) situé dans le dossier db/dump.sql, créez vos tables et remplissez les.

Un paramètrage au niveau de la connection de l'application à la base de données est maintenant nécessaire : dans le dossier app/config, il y a deux fichiers : dev et prod (correspondant ainsi a l'environnement)

Il faudra y inscrire vos logs et votre hôte afin que l'application puisse s'y connecter.

+ **L'URL**

Afin d'accéder à l'application, il est très important de spécifier dans l'URL le fichier d'entrée de l'application.

Si vous avez par exemple un dossier www/ dans un wampserver (qui s'appelerait Formaweb), l'URL serait : localhost/Formaweb/web/index.php

Même en cas d'URL re-writing, il faut toujours spécifier le fichier index.php dans l'url.

## Contexte

L'entreprise M2L souhaiterait une application web (un site) pour permettre à ses clients de consulter les formations dispensées par divers organismes dans les locaux M2L. Ces formations doivent être présentées sous forme d'articles, avec possibilité de commenter ces articles.

Ces commentaires et la gestion des articles et des comptes demandent donc une connexion pour les utilisateurs (pour commenter) et pour des administrateurs afin de créer, modifier ou supprimer des articles ou comptes grâce à un panel administratif type CRUD.

L'application possède aussi un web service (comme l'autre application Silex) qui permet de créer des .json contenant les formations présentes dans la base de données de l'application Formaweb.